FROM python:3.7-alpine

RUN echo "************** updating APK ... **************" && \
    apk add --update git && \
    echo "************** Installing dependencies ... **************" && \
    apk add build-base libxml2 linux-headers libxml2-dev libxslt-dev python-dev && \
    echo "************** Cloning repo ... **************" && \
    git clone https://gitlab.com/datopian/ckan-ng-harvest.git && \
    cd ckan-ng-harvest && \
    pwd && \
    ls -la && \
    echo "************** Installing pip requirements ... **************" && \
    pip install -U pip && \
    pip install pyproj==1.9.6 && \
    pip install --no-cache-dir -r requirements.txt